component {
	function configure() {
		coldbox = {
			reinitPassword: "",
			customErrorTemplate: "/coldbox/system/includes/BugReport.cfm"
		};

		// wirebox integration
		wirebox = {
			binder = 'config.WireBox',
			singletonReload = true,
			handlersIndexAutoReload = true
		};

		moduleSettings = {
			quick = {
				defaultGrammar: "MySQLGrammar"
			},
			qb = {
				defaultGrammar: "MySQLGrammar",
				datasource: "cfShorty"
			}
		};
	}
}