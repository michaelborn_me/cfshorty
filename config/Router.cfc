// Inherits from Router and FrameworkSuperType
component {

	function configure(){
		// Turn on Full URL Rewrites, no index.cfm in the URL
		setFullRewrites( true );

		route( "/r/:shortURI" ).to( "shorty.redirect" );

		resources( resource="urls" );

		// setup convention routing
		route( ":handler/:action?").end();
	}
}