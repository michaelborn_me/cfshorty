# cfShorty

A super-simple URL shortener built in CFML and using HSQLDB for a flat-file database.

```js
this.datasources["hsqldb"] = {
    class: 'org.hsqldb.jdbcDriver'
    , connectionString: 'jdbc:hsqldb:file:#expandPath("/PATH/TO/FILE")#'
};
```

* Build html form to manage urls
* save url in flat-file DB.
* when url is hit, pull out long url and redirect to it.

* install Coldbox
* create basic bulma layout for admin screen
* create bulma form with one input and one save button - should post to /manage/urls
* create admin handler and model to list urls - /manage/urls, index() function.
* create bulma data table to list URLs from prc.data. - grab table output view from cfstocky app?
* create handler for short url redirects - e.g. redirect /Eg341Z to http://michaelborn.me

## Managing URLs

URLs are managed at `/urls`.

Form should have a single input: URL.

When hit save button, generates an 6-digit random character string and saves to lucee cache.

## Redirecting URLs

When hit `/gX34GZ`, search in lucee cache, pull out long URL, and redirect 302.