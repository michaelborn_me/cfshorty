/**
 * Manage urls
 * It will be your responsibility to fine tune this template, add validations, try/catch blocks, logging, etc.
 */
component extends="BaseHandler" {
	
	// DI 
	
	// HTTP Method Security
	this.allowedMethods = {
		index  		= "GET", 
		new    		= "GET", 
		create   	= "POST,PUT", 
		show 		= "GET", 
		edit		= "GET",
		update 		= "POST,PUT,PATCH",
		delete 		= "DELETE"
	};
	
	/**
	 * Param incoming format, defaults to `html`
	 */
	function preHandler( event, rc, prc ){
		event.paramValue( "format", "html" );
	}
		
	/**
	 * Display a list of urls
	 */
	function index( event, rc, prc ){
		// Get resources here
		prc.data = getInstance( "Uri" ).all();

		event.setView( "urls/index" );
	}

	/**
	 * Return an HTML form for creating one URL
	 */
	function new( event, rc, prc ){
		event.paramPrivateValue( "data", {
			longURI: ""
		} );
		event.setView( "urls/new" );
	}

	/**
	 * Create a urls
	 */
	function create( event, rc, prc ){
		event.paramValue( "longURI", "" );
		// TODO: VALIDATE - longURI exists and is not empty.
		getInstance( "Url" ).new( event.getValue( "longURI" ) );

		relocate( "urls" );
	}

	/**
	 * Show a URL
	 */
	function show( event, rc, prc ){
		event.paramValue( "id", 0 );
		prc.data = getInstance( "Url" ).getByShortURI( event.getValue( "id") );
		
		event.setView( "urls/show" );
	}

	/**
	 * Edit a URL
	 * Incoming id is set in URL via resource routes - `/urls/:id`
	 */
	function edit( event, rc, prc ){
		event.paramValue( "id", 0 );
		prc.data = getInstance( "Url" ).getByShortURI( event.getValue( "id" ) );
		
		event.setView( "urls/edit" );
	}

	/**
	 * Update a URL
	 */
	function update( event, rc, prc ){
		// TODO: VALIDATE - shortURI exists and matches an existing row.
		// TODO: VALIDATE - longURI exists and is not empty.
		getInstance( "Url" )
			.update( 
				shortURI = event.getValue( "id" ),
				longURI = event.getValue( "longURI", "" )
			);
		relocate( "urls" );
	}

	/**
	 * Delete a urls
	 */
	function delete( event, rc, prc ){
		event.paramValue( "id", 0 );
		getInstance( "Url" )
			.deleteByShortURI( event.getValue( "id" ) );
		relocate( "urls" );
	}
	
}
