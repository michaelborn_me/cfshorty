/**
* I handle /r/ requests with short unique strings and redirect them.
*/
component extends="BaseHandler" {
		
	function redirect( event, rc, prc ){
		prc.data = getInstance( "Url" ).getByShortURI( event.getValue( "shortURI", "" ) );

		if ( prc.data.recordCount ){
			relocate( url = prc.data.longURI );
		} else {
			event.setView( "shorty/notfound" );
		}
	}	
}