component {
    
    function up( schema, query ) {
        schema.create( "uris", function( table ){
            table.increments( "id" );
            table.string( "shortURI" );
            table.string( "longURI" );
            table.timestamp( "createdDate" );
        } );
    }

    function down( schema, query ) {
        
    }

}
