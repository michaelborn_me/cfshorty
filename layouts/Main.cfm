<cfoutput>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>cfShorty URL Shortener</title>
	<meta name="description" content="URL Shortener">
	<meta name="author" content="Michael Born">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css" integrity="sha256-vK3UTo/8wHbaUn+dTQD0X6dzidqc5l7gczvH+Bnowwk=" crossorigin="anonymous" />
</head>
<body>

	<!---Container And Views --->
	<div class="container">#renderView()#</div>

</body>
</html>
</cfoutput>
