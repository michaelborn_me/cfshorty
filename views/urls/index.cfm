<cfoutput>
	<h1 class="title">Manage URLs</h1>
	#renderView( "urls/nav" )#
	<table class="table" style="width:100%;">
		<thead>
			<tr>
				<th>Short URL</th><th>Long URL</th><th>Action</th>
			</tr>
		</thead>
		<tbody>
			<cfoutput query="#prc.data#">
				<tr>
					<td><a href="#event.buildLink( 'r.#prc.data.shortURI#' )#">#prc.data.shortUri#</a></td>
					<td>#prc.data.longUri#</td>
					<td><a href="#event.buildLink( 'urls.#prc.data.shortUri#.edit' )#">Edit</a> / <a href="#event.buildLink( 'urls.#prc.data.shortUri#?_method=DELETE' )#">Delete</a></td>
				</tr>
			</cfoutput>
		</tbody>
	</table>
</cfoutput>