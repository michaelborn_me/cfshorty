<cfoutput>
	<div class="field">
		<label class="label" for="longUri">Long URL</label>
		<input type="url" name="longUri" placeholder="http://michaelborn.me" value="#encodeForHTMLAttribute( prc.data.longURI )#" />
	</div>
	<div class="field">
		<input type="submit" class="button is-primary" value="Save" />
	</div>
</cfoutput>