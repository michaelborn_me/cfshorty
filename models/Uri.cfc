/**
* I am a URI entity which represents the URIs table
*/
component accessors="true" extends="quick.models.BaseEntity"{
	
	// Properties
	property name="id";
	property name="shortURI";
	property name="longURI";
	property name="createdDate" cfsqltype="timestamp";
	

	/**
	 * Constructor
	 */
	public component function init(){
		return this;
	}
	

}