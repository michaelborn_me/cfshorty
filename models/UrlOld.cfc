/**
 * 
*/
component {

	public component function init(){
		return this;
	}

	/**
	 * Create and save a new URL.
	 * @param {Integer} longURI - The long URL version
	*/
	function new( required string longURI ){
		// save to DB...
		var sql = "
			INSERT INTO URIs ( shortURI, longURI )
			VALUES ( :shortURI, :longURI )
		";
		var randomEightDigitString = getUniqueRandomString();
		var params = {
			shortURI: { value: randomEightDigitString, cfsqltype: "varchar" },
			longURI: { value: arguments.longURI, cfsqltype: "varchar" }
		};
		return queryExecute( sql, params );
	}

	/**
	 * Update an existing URL.
	 * @param {Integer} shortURI - The short URL version. This acts as the primary key for the record.
	 * @param {Integer} longURI - The long URL version
	*/
	function update( required string shortURI, required string longURI ){
		// save to DB...
		var sql = "
			UPDATE URIs set longURI=:longURI
			WHERE shortURI=:shortURI
		";
		var params = {
			shortURI: { value: arguments.shortURI, cfsqltype: "varchar" },
			longURI: { value: arguments.longURI, cfsqltype: "varchar" }
		};
		return queryExecute( sql, params );
	}

	/**
	 * DELETE an existing url by primary key.
	 * @param {Integer} shortURI - The short URL version. This acts as the primary key for the record.
	*/
	function deleteByShortURI( required string shortURI ){
		var sql = "
			DELETE FROM URIs WHERE shortURI=:shortURI
		";
		var params = {
			shortURI: { value: arguments.shortURI }
		};
		return queryExecute( sql, params );
	}

	/**
	 * Generate a unique string for purposes of the short URI.
	 * @param {Integer} stringLength - Specify the length of random string we want.
	 * @returns {String} randomEightDigitString - exactly 10 characters long
	*/
	private string function getUniqueRandomString( numeric stringLength = 10 ){
		var uniqueString = application.wirebox.getInstance( "Util" ).getRandString( arguments.stringLength );

		if ( getByShortURI( uniqueString ).recordCount ){
			uniqueString = getUniqueRandomString();
		}

		return uniqueString;
	}

	/**
	 * Select all URLs in the database.
	 * @returns {Query} - all URLs in the db table.
	*/
	public query function getAll( ){
		var sql = "
			SELECT shortURI,longURI, createdDate
			FROM URIs
			ORDER BY createdDate DESC
		";
		return queryExecute( sql, {} );
	}

	/**
	 * Select all URLs matching the given short URI
	 * @param {String} shortURI - Filter by this unique value.
	 * @returns {Query} - single row at most which matches the unique short uri.
	*/
	public query function getByShortURI( required string shortURI ){
		var sql = "
			SELECT shortURI,longURI FROM URIs
			WHERE shortURI=:shortURI
		";
		var params = {
			shortURI: { value: arguments.shortURI }
		};
		return queryExecute( sql, params );
	}
}